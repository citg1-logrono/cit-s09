package com.zuitt.discussion;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;

@SpringBootApplication
@RestController
@RequestMapping("/greetings")
public class DiscussionApplication {

	public static void main(String[] args) {
		SpringApplication.run(DiscussionApplication.class, args);
	}


	@GetMapping("/hello")
	public String hello() {
		return "Hello World";
	}

	@GetMapping("/hi")
	public String hi(@RequestParam(value = "name", defaultValue = "John") String name) {
		return String.format("Hi %s", name);
	}

	@GetMapping("/friend")
	public String friend(@RequestParam(value = "name", defaultValue = "Joe") String name, @RequestParam(value = "friend", defaultValue = "Jane") String friend) {
		return String.format("Hello %s My name is %s.", friend, name);
	}

	// Route with path variables
	// Dynamic is obtained directly from the url
	// localhost:8080/name
	@GetMapping("/hello/{name}")
	// "PathVariable" annotation allows us to extract data directly from the URL
	public String greetFriend(@PathVariable("name") String name) {
		return String.format("Nice to meet you %s!", name);
	}

// Activity

	ArrayList<String> enrollees = new ArrayList<String>();

	@GetMapping("/enroll")
	public String enroll(@RequestParam(value = "user", defaultValue = "Brett") String user) {
		enrollees.add(user);
		return String.format("Thank you for enrolling, %s!", user);
	}

	@GetMapping("/getEnrollees")
	public String Enrollees() {
		return String.format(String.valueOf(enrollees));
	}

	@GetMapping("/nameage")
	public String nameage(@RequestParam(value = "name", defaultValue = "Brett") String name, @RequestParam(value = "age", defaultValue = "0") int age) {
		return String.format("Hello %s, My age is %d.", name, age);
	}

	@GetMapping("/courses/{id}")
	public String courses(@PathVariable("id") String id) {
		switch(id){
			case "java101":
				return String.format("Name: Java 101,Schedule: MWF 8:00AM-11:00AM, Price: PHP 3000.00");
			case "sql101":
				return String.format("Name: SQL 101, Schedule: TTH 1:00PM-04:00PM, Price: PHP 2000.00");
			case "javaee101":
				return String.format("Name: Java EE 101, Schedule: MFW 1:00PM-04:00PM, Price: PHP 3500.00");
			default:
				return String.format("Course cannot be found.");
		}
	}

}

